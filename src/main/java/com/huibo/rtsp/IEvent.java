/*
  Copyright (C), 2009-2020, 江苏汇博机器人技术股份有限公司
  FileName: IEvent
  Author:   ShuangPC
  Date:     2020/9/4
  Description: IEvent.java 网络事件处理器，当Selector可以进行操作时，调用这个接口中的方法
  History:
  <author>         <time>          <version>          <desc>
  作者姓名         修改时间           版本号             描述
 */

package com.huibo.rtsp;

import java.io.IOException;
import java.nio.channels.SelectionKey;

/**
 * IEvent.java 网络事件处理器，当Selector可以进行操作时，调用这个接口中的方法
 *
 * @author ShuangPC
 * @date 2020/9/4
 */

public interface IEvent {
    /**
     * 当channel得到connect事件时调用这个方法.
     *
     * @param key
     * @throws IOException
     */
    void connect(SelectionKey key) throws IOException;

    /**
     * 当channel可读时调用这个方法.
     *
     * @param key
     * @throws IOException
     */
    void read(SelectionKey key) throws IOException;

    /**
     * 当channel可写时调用这个方法.
     *
     * @throws IOException
     */
    void write() throws IOException;

    /**
     * 当channel发生错误时调用.
     *
     * @param e
     */
    void error(Exception e);
}
